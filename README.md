# MyProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.2.

## Requirment:
Installed node v7.8.0 and npm 5.3.0 version for Angular CLI setup.

## Setup
Get the clone and go to folder (myProject)

## Installed all dependencies
npm install
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


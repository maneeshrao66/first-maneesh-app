import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.html',
    styleUrls: ['./home.scss']
})
export class HomeComponent implements OnInit {
    public allFeatures: any = [];
    public businessPlan: any = [];
    public allOnIncludedData: any = [];
    public selectedPlan: number;
    public planValue: number;
    public totalPlanAmount: number;
    public priceValue: number;

    constructor() {
        this.planValue = 0;
        this.totalPlanAmount = 0;
    }

    ngOnInit() {
        this.businessPlan = [
            {
                'name': 'PERSONAL',
                'icon': 'wallet.png',
                'value': 28,
                'planName': 'Made for small businesses (up to 10 devices)',
                'planType': 'Free for 14 days, No credit card needed',
                'buttonName': '$28 PER DEVICE / PER YEAR'
            },
            {
                'name': 'STARTER',
                'icon': 'helmet.png',
                'value': 24,
                'planName': 'Perfect for any startups (11-50 devices)',
                'planType': 'Free for 14 days, No credit card needed',
                'buttonName': '$24 PER DEVICE / PER YEAR'
            },
            {
                'name': 'PRO',
                'icon': 'pro.png',
                'value': 18,
                'planName': 'Ideal for mid-sized businesses (51-100 devices)',
                'planType': 'Free for 14 days, No credit card needed',
                'buttonName': '$18 PER DEVICE / PER YEAR'
            },
            {
                'name': 'BUSINESS',
                'icon': 'suitcase_travel.png',
                'value': 12,
                'planName': 'Best value for growing Enterprises (above 100 devices)',
                'planType': 'Free for 14 days, No credit card needed',
                'buttonName': '$12 PER DEVICE / PER YEAR'
            },
        ];
        this.allFeatures = [
            'Kiosk Mode' ,
            'Custom Branding',
            'Unlimited User Accounts and Role based Access',
            'Two-factor Authentication Support',
            'Location Tracking & History Retention (90 days)',
            'Kiosk Browser',
            'Bulk Enrollment of Devices',
            'Reports and Alerts (for 90 days)',
            'World Class Customer Support',
            'Dedicated Account manager',
        ];
        this.allOnIncludedData = [
            {
                'name': 'Enterprise Store',
                'icon': 'windows-firewall.png',
                'description': 'All features are included',
                'storage': '200 MB STORAGE',
            },
            {
                'name': 'Content Management',
                'icon': 'content.png',
                'description': 'All features are included',
                'storage': '200 MB STORAGE',
            },
            {
                'name': 'Remote Cast',
                'icon': 'remote-cast.png',
                'description': 'All features are included',
                'storage': 'UNLIMITED SESSION',
            },
            {
                'name': 'EVA - Business Messenger',
                'icon': 'messanger.png',
                'description': 'All features are included',
                'storage': 'INCLUDED IN PLAN',
            },
        ];
    }
    public selectPlan(value, i) {
        this.selectedPlan = i;

        this.planValue = value;
    }
    public calculateAmount() {
        this.totalPlanAmount = this.planValue * this.priceValue;
    }

}

import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {BlogComponent} from './blog/blog.component';
import {DemoComponent} from './demo/demo.component';
import {PricingComponent} from './pricing/pricing.component';

export const router: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'blog', component: BlogComponent},
    {path: 'demo', component: DemoComponent},
    {path: 'pricing', component: PricingComponent},
    {path: '**', redirectTo: 'home', pathMatch: 'full'}
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-demo',
    templateUrl: 'demo.html',
    styleUrls: ['./demo.scss']
})
export class DemoComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
